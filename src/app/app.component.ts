import { Component, OnInit } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../../node_modules/bootstrap/dist/css/bootstrap.css']
})
export class AppComponent {
  title = 'fisrt-app';
  userList: User[] = [];
  
  addUser(frm) {  
    var user: User;
    user= frm.value;
    this.userList.push(user);
    
    console.log(frm.value); 
    
  }
  
}
 