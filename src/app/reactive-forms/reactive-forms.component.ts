import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.css', '../../../node_modules/bootstrap/dist/css/bootstrap.css']
})
export class ReactiveFormsComponent implements OnInit {

  userList: User[]=[];
  form: FormGroup;
  
  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [ Validators.required ] ), //validators as second parameter
      contact: new FormControl('', [ Validators.required, Validators.pattern("[0-9]+")] ),
      email: new FormControl('', [ Validators.required, Validators.email ] ) // validators can be put in a list 
    })
  }
  
  
  addUser(form) {
    console.log(form.value); //this approach uses the parameter passed to access value (optinal in reactive form).
    console.log(this.form.value); // this approach uses the formgroup instance to capture values.
    this.userList.push(this.form.value);
  }
}
